import unittest
from app import app

class TestAppUtil(unittest.TestCase):
    def test_add(self):
        client = app.test_client()
        result = int(client.get('/add', query_string={'a':2, 'b':3}).data)
        self.assertEqual(result, 5)

    def test_sub(self):
        client = app.test_client()
        result = int(client.get('/sub', query_string={'a':3, 'b':1}).data)
        self.assertEqual(result, 2)

if __name__ == "__main__":
    unittest.main()
        