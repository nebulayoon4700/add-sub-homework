from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Bob from sungyoon cho'

@app.route('/add')
def add():
    try:
        a = int(request.args.get('a'))
        b = int(request.args.get('b'))
    except:
        return "input a and b"
    return str(a + b)

@app.route('/sub')
def sub():
    try:
        a = int(request.args.get('a'))
        b = int(request.args.get('b'))
    except:
        return "input a and b"
    return str(a - b)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8165)